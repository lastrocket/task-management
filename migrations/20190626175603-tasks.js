module.exports = {
  up(db) {
    return db.createCollection('tasks');
  },
  down(db) {
    return db.collection('tasks').drop();
  }
};
