module.exports = {
  up(db) {
    return db.createCollection('projects');
  },
  down(db) {
    return db.collection('projects').drop();
  }
};
