const express = require("express");
const router = express.Router();
const passport = require("passport");

const Task = require("../../models/Task");

// @route GET api/tasks/:id
// @desc Get tasks for specific project
// @access Private
router.get(
  "/:projectId/isAll/:isAll",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let {projectId, isAll} = req.params;
    if(isAll === 'true') {
      Task.find({ project: projectId }).then(tasks => {
        return res.json(tasks)
      });
    } else {
      Task.find({ project: projectId, assignee: req.user.email }).then(tasks => {
        return res.json(tasks)
      });
    }
    
  }
);

// @route GET api/tasks/user/:email
// @desc Get tasks for user
// @access Private
router.get(
  "/:email/user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Task.find({ assignee: req.params.email }).then(tasks => res.json(tasks));
  }
);

// @route POST api/tasks/create
// @desc Create a new task
// @access Private
router.post(
  "/create",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const NEW_TASK = new Task({
      project: req.body.project,
      taskName: req.body.taskName,
      dateDue: req.body.dateDue,
      assignee: req.body.assignee
    });

    NEW_TASK.save()
      .then(task => res.json(task))
      .catch(err => console.log(err));
  }
);

// @route PUT api/tasks/:id
// @desc Update a task
// @access Private
router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    // console.log(req.body);
    
    Task.findOneAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true }
    )
      .then(task => {
        res.json(task);
      })
      .catch(err => console.log(err));
  }
);

// @route Delete api/tasks/:id
// @desc Delete a task
// @access Private
router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Task.findById(req.params.id)
      .then(task => {
        task.deleteOne().then(() => 
          res.status(202).json({ success: true, deletedId: req.params.id})
        );
      })
      .catch(err => console.log(err));
  }
);

module.exports = router;
