import axios from "axios";

import {
  CREATE_TASK,
  UPDATE_TASK,
  DELETE_TASK,
  GET_TASKS,
  GET_TASKS_BY_USER,
  TASKS_LOADING
} from "./types";

// Create Task
export const createTask = (taskData, user, ownerEmail) => dispatch => {
  console.log(user.email);
  
  axios
    .post("/api/tasks/create", taskData)
    .then(res => {
      console.log(user.email, ownerEmail);
      
      (res.data.assignee === user.email || user.email === ownerEmail) && dispatch({
        type: CREATE_TASK,
        payload: res.data
      })
    }
      
    )
    .catch(err => console.log(err));
};

// Get tasks by project id
export const getTasks = (projectId, isAll) => dispatch => {
  dispatch(setTasksLoading());
  axios
    .get(`/api/tasks/${projectId}/isAll/${isAll}`)
    .then(res =>
        dispatch({
          type: GET_TASKS,
          payload: res.data
        })
    )
    .catch(err =>
      dispatch({
        type: GET_TASKS,
        payload: null
      })
    );
};

// Get tasks by user
export const getTasksByUser = email => dispatch => {

  dispatch(setTasksLoading());
  axios
    .get(`/api/tasks/${email}/user`)
    .then(res =>{      
      dispatch({
        type: GET_TASKS_BY_USER,
        payload: res.data
      })
      console.log(res, 99999999);}
    )
    .catch(err =>
      dispatch({
        type: GET_TASKS_BY_USER,
        payload: null
      })
    );
}

// Get tasks by project id
export const updateTask = (data) => dispatch => {
  dispatch({
    type: UPDATE_TASK,
    payload: data
  })
  axios
    .put(`/api/tasks/${data._id}`, data)
    .then(res => 
      {
        return res;
      }
    )
    .catch(err =>
      dispatch({
        type: GET_TASKS,
        payload: null
      })
    );
};

// Get tasks by project id
export const deleteTask = (id) => dispatch => {
  axios
    .delete(`/api/tasks/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_TASK,
        payload: res.data.deletedId
      })
      return res;
    })
    .catch(err =>
      dispatch({
        type: GET_TASKS,
        payload: null
      })
    );
};

// Tasks loading
export const setTasksLoading = () => {
  return {
    type: TASKS_LOADING
  };
};
