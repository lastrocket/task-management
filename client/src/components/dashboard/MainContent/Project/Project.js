import React, { Component } from "react";
import { connect } from "react-redux";
import { getProject } from "../../../../actions/projectsActions";
import { getTasks, updateTask, deleteTask } from "../../../../actions/taskActions";

import Spinner from "../../../common/Spinner";
import Modal from "../Modal/Modal";

import "../MainContent.scss";
import "./Project.scss";

class Project extends Component {
  state = {
    modal: false,
    edit: false,
    editTask: false,
    addTask: false,
    task: {},
    name: "",
    members: [],
    id: "",
    owner: {},
    tasks: [],
    date: ""
  };

  closeModal = e => {
    this.setState({ 
      modal: !this.state.modal,
      edit: false, 
      addTask: false,
      editTask: false,
    });
  };

  toggleEditModal = (name, members, id, owner, e) => {
    this.setState({
      modal: !this.state.modal,
      edit: !this.state.edit,
      name: name,
      members: members,
      id: id,
      owner: owner
    });
  };

  toggleTaskModal = e => {
    this.setState({
      modal: !this.state.modal,
      addTask: true,
      editTask: false,
    });
  };

  toggleEditTaskModal = task => {
    this.setState({
      task,
      editTask: true,
      modal: !this.state.modal,
      addTask: false,
    });
  };

  componentDidMount() {
    this.props.getProject(this.props.match.params.project);
    this.props.getTasks(this.props.match.params.project);
    console.log(this.props, 999);
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.project !== prevProps.match.params.project 
      || this.props.project.length !== prevProps.project.length) {
      this.props.getProject(this.props.match.params.project);
      let isTrue = this.props.auth.user.id === this.props.project.owner.id;
      this.props.getTasks(this.props.match.params.project, isTrue);
        console.log(isTrue, 888);
    }
  }

  onChange = async (e, task, isInput) => {
    isInput === 'inp' ? task.taskName = e.target.value : task.taskCompleted = +e.target.value;
    this.props.updateTask(task);

  };
  deleteTask = id => {
    let isTrue = window.confirm("Are you sure you want to delete this task ?");
    isTrue && this.props.deleteTask(id);
  }
  render() {
    const { tasks } = this.props.tasks;
    let tasksList = tasks.map((task, index) => (
      <div className="task-input" key={index}>
        <select 
          value={task.taskCompleted} 
          onChange={(e) => this.onChange(e, task, 'sel')}
          style={{marginRight: '15px'}}
        >
          <option value='0'>To do</option>
          <option value='1'>In progress</option>
          <option value='2'>Done</option>
        </select>
        <input
          type="text"
          name="task"
          id={task._id}
          value={task.taskName}
          onChange={(e) => this.onChange(e, task, 'inp')}
          className="project-task"
        />
        <span className={!task.assignee ? "task-info muted" : "task-info"}>
          {task.assignee === this.props.auth.user.email
            ? "You"
            : task.assignee || "Unassigned"}
        </span>
        <span
          className={
            task.dateDue === "Date undefined" ? "task-info muted" : "task-info"
          }
        >
          {task.dateDue === "Date undefined" ? "Not Set" : task.dateDue}
        </span>
        <i className="material-icons edit" onClick={() => this.toggleEditTaskModal(task)}>
          edit
        </i>
        <i className="material-icons" onClick={() => this.deleteTask(task._id)}>
          delete_outline
        </i>
      </div>
    ));

    if (
      this.props.project &&
      this.props.project.teamMembers &&
      !this.props.projects.projectLoading &&
      !this.props.tasks.tasksLoading
    ) {
      const { project } = this.props;
      return (
        <div className="main-content">
          <h1 className="project-header">{project.name}</h1>
          <button
            onClick={this.toggleEditModal.bind(
              this,
              project.name,
              project.teamMembers,
              project._id,
              project.owner
            )}
            className="main-btn center-btn"
          >
            Edit Project Info
          </button>

          <div className="modal-wrapper">
            <Modal
              onClose={this.closeModal}
              modal={this.state.modal}
              edit={this.state.edit}
              editTask={this.state.editTask}
              addTask={this.state.addTask}
              task={this.state.task}
              name={this.state.name}
              members={this.state.members}
              id={this.state.id}
              owner={this.state.owner}
            />
          </div>
          <div className="tasks-container">
            <div className="projects-first-row">
              <button
                className="main-btn add-btn"
                onClick={this.toggleTaskModal}
              >
                Add task
              </button>
              <div className="projects-column-headers">
                <p>Assignee</p>
                <p>Due</p>
              </div>
            </div>
            <div className="project-tasks">{tasksList}</div>
          </div>
        </div>
      );
    }

    return (
      <div className="project-spinner">
        <Spinner />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  project: state.projects.project,
  projects: state.projects,
  tasks: state.tasks
});

export default connect(
  mapStateToProps,
  { getProject, getTasks, updateTask, deleteTask }
)(Project);
