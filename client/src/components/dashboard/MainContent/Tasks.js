import React, { Component } from "react";
import "./MainContent.scss";
import { connect } from "react-redux";
import { getTasksByUser } from "../../../actions/taskActions";

import Modal from "./Modal/Modal";

class Tasks extends Component {
  state = {
    modal: false
  };

  toggleModal = e => {
    this.setState({ modal: !this.state.modal });
  };
  componentDidMount() {
    
    this.props.getTasksByUser(this.props.user.email)
  }
  render() {
    const { user } = this.props;
    const { tasks } = this.props.tasks.tasks;
    console.log(this.props, 5656);
    
    let tasksList = tasks.map((task, index) => (
      <div className="task-input" key={index}>
        <span className='task-info muted plafqamich'>{task.taskCompleted === 0 ? 'To do' : task.taskCompleted === 1 ? 'In progress' : 'Done' }</span>
        <span className='task-info muted plafqamich'>{task.taskName}</span>
        <span className={!task.assignee ? "task-info muted plafqamich" : "task-info plafqamich"}>
          {task.assignee === user.email
            ? "You"
            : task.assignee || "Unassigned"}
        </span>
        <span
          className={
            task.dateDue === "Date undefined" ? "task-info muted plafqamich" : "task-info plafqamich"
          }
        >
          {task.dateDue === "Date undefined" ? "Not Set" : task.dateDue}
        </span>
      </div>
    ));
    return (
      <div className="main-content">
        <h1 className="header">Your Tasks</h1>
        <div className="projects">
          <div className="no-projects">
            <div className="project-tasks" style={{width: '100%'}}>
              {tasks.length > 0 && (
              <React.Fragment>
                <div className="task-input">
                  <span className='task-info muted plafqamich'><b>Status</b></span>
                  <span className='task-info muted plafqamich'><b>Task Name</b></span>
                  <span className='task-info muted plafqamich'><b>Creator</b></span>
                  <span className='task-info muted plafqamich'><b>Due</b></span>
                </div>
                {tasksList}
                </React.Fragment>

              )
            }
            </div>
            {!tasksList.length && <h1 className="header">You have no tasks</h1>}
            {/* {tasksList.length > 0 ? (
              <p>Visit a project to create your first task</p>
            ) : (
              <button className="main-btn" onClick={this.toggleModal}>
                Create your first project
              </button>
            )} */}
            
            <Modal onClose={this.toggleModal} modal={this.state.modal} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({projects: state.projects.projects, user: state.auth.user , tasks: state})

export default connect(
  mapStateToProps,
  {getTasksByUser}
)(Tasks);
