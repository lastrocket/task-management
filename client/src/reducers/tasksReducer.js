import {
  CREATE_TASK,
  UPDATE_TASK,
  DELETE_TASK,
  GET_TASKS,
  GET_TASKS_BY_USER,
  TASKS_LOADING
} from "../actions/types";

const initialState = {
  tasks: [],
  tasksLoading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATE_TASK:
      return {
        ...state,
        tasks: [action.payload, ...state.tasks]
      };
    case UPDATE_TASK:
      let index = state.tasks.findIndex(
        task => task._id === action.payload._id
      );
      state.tasks.splice(index, 1);
      let newArr = [...state.tasks];
      newArr.splice(index,0,action.payload)
      return {
        ...state,
        tasks: newArr,
        task: action.payload,
      };
    case DELETE_TASK:
      return {
        ...state,
        tasks: state.tasks.filter(
          task => task._id !== action.payload
        )
      };
    case GET_TASKS:
      return {
        ...state,
        tasks: action.payload,
        tasksLoading: false
      };
    case GET_TASKS_BY_USER:
      return {
        ...state,
        tasks: action.payload,
        tasksLoading: false
      };
    case TASKS_LOADING:
      return {
        ...state,
        tasksLoading: true
      };
    default:
      return state;
  }
}
